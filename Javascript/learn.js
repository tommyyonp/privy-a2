// Console Log
alert('Hello World');
console.log('Hello World');
console.error('This is an error');
console.warn('This is a warning');

// Variables: var, let, const
let number = 30;

// let can be re-assigned, const can not
number = 31;

// Data type: String, Number, Boolean, null, undefined, Symbol

const fullName = 'John johny yes papa';
const age = 30;
const isCool = true;
const rating = 4.5;
const a = null;
const b = undefined;
let z;

// Checking type
console.log(typeof age)

// Concatenation
console.log('Hello my name is ' + fullName + ' and i ' + age + ' years old')

// Template String (better than concatenation)
const hello = `Hello my name is ${fullName} and i ${age} years old`
console.log(hello);

const s = 'Technology, Computer, IT, Code';

console.log(s.length);
console.log(s.toUpperCase());
console.log(s.toLowerCase());
console.log(s.substring(0, 5));
console.log(s.substring(0,5).toUpperCase());
console.log(s.split(','));

// Arrays - Variables that hold multiple values

const numbers = new Array(1, 2, 3, 4, 5);
console.log(numbers)
 
const fruits = ['Apple', 'Orange', 'Avocado ', 10, true]
fruits[5] = 'Grape';

// Add to the last of array
fruits.push = 'Mango';

// Add to the first of array
fruits.unshift = 'Strawberry';

// Remove the last array
fruits.pop() 

// Check is it array or not
console.log(Array.isArray(fruits))
 
// Check index in array
console.log(fruits.indexOf('Apple'))
 
console.log(fruits)

// Object Literals
const person = {
    firstName: 'John',
    lastName: 'Yes Papa',
    age: 20,
    hobbies: ['Music', 'Sport', 'Movies'],
    address: {
        street: '50 main st',
        city: 'Boston',
        state: 'MA'
    }
}

// Console some data
console.log(person.firstName, person.lastName)

 // Console array in hobbies 
 console.log(person.hobbies[1])

 // Console city in adress
 console.log(person.address.street)

 // Pull some data out
const {firstName, lastName, address: {city}} = person;
console.log(city) 

 // Add some data
 person.email = 'john@gmail.com'
console.log(person.email) 

// Array of object and JSON
const todos = [
    {
        id: 1,
        text: 'Take out trash',
        isCompleted: true 
    },
    {
        id: 2,
        text: 'Meeting with bos',
        isCompleted: false 
    },
    {
        id: 3,
        text: 'Dentist appt',
        isCompleted: true 
    },
];
// Console log todos
console.log(todos)

// Console log text in array
console.log(todos[1].text)

// Convert array to JSON
const todoJSON = JSON.stringify(todos);
console.log(todoJSON)

// About Loops

// Using while
let i = 0;
while (i <= 10) {
    console.log(`While Loop Number: ${i}`);
    i++;
}

// Using for
for (let i = 0; i <= 10; i++) {
    console.log(`For Loop Number: ${i}`)
}

for (let todo of todos) {
    console.log(todo.text)
}

// Using forEach, map, filter
todos.forEach((todo) => {
    console.log(todo.text)
})

const todoText = todos.map((todo) => {
    return todo.text
})
console.log(todoText)

const completedTodo = todos.filter((todo) => {
    return todo.isCompleted === true
}).map((todo) => {
    return todo.text
})
console.log(completedTodo)

// Conditional

const x = 4;
const y = 10;

if (x === 10) {
    console.log("x is 10")
} else if (x > 10) {
    console.log("x is greater than 10")
} else {
    console.log("x is less than 10")
}

// Ternary Operation
const n = 10
const color = n > 5 ? 'red' : 'blue'
console.log(color)

switch(color) {
    case 'red':
        console.log('color is red')
        break
    case 'blue':
        console.log('color is blue')
        break
    default:
        console.log('color is NOT red or blue')
        break
}

// About Function
function addNumbs(num1 = 1, num2 = 1) {
    return num1 + num2
}

const addNumber = (num1 = 1, num2 = 2) => {
    return num1 + num2
}

const addNumbers = (num1 = 3, num2 = 2) => num1 + num2
 
console.log(addNumbs( ))
console.log(addNumber())
console.log(addNumbers())


function Person(fName, lName, dob) {
    this.fName = fName
    this.lName = lName
    this.dob = new Date(dob)
}

Person.prototype.getFullName = function() {
    return `${this.fName} ${this.lName}`
}

class Person {
    constructor(fName, lName, dob) {
        this.fName = fName
        this.lName = lName
        this.dob = new Date(dob)
    }

    getFullName() {
        return `${this.fName} ${this.lName}`
    }
}

const person1 = new Person('John', 'Doe', '4-3-1990')
const person2 = new Person('Mary', 'Smith', '3-6-1970'
)

console.log(person2.getFullName())
console.log(person1)
